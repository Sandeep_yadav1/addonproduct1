
@extends('layouts.navbar')



@section('content')
<script src=
"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
      </script>
  
        <!--These jQuery libraries for 
           chosen need to be included-->
        <script src=
"https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js">
        </script>
        <link rel="stylesheet" 
              href=
"https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css" />
  
        <!--These jQuery libraries for select2 
            need to be included-->
        <script src=
"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js">
       </script>
        <link rel="stylesheet" 
              href=
"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" />

<a href="" class="btn btn-primary" style="border-radius:20px;margin:10px 480px">Back to Addons List</a>
    <a href="#" class="btn btn-primary" style="border-radius: 20px;">Attach Addons to the Another Product</a>

    <div class="card " style="width:1050px;height:300px;margin-left:450px">
  <div class="card-header">
    Product Addons
  </div>  
  <div class="card-body">
  <h5 class="card-title">Select Product (Master)</h5>
  <?php ?> 
  <select class="form-control" name="product_id" disabled>
   
   <option>{{$dataformate['title']}}</option>

   <?php echo json_encode($dataformate); ?>
   
 </select>
  
  <br><br>
    <h5 class="card-title">Select Addons Products (Child's)</h5> 
    <meta name="csrf-token" content="{{ csrf_token() }}">
<select id="js-example-data-array1" class="js-example-responsive" multiple="multiple" style="width: 100%" name="child[]"></select>

  </div>
 
</div>

<script>
  let selected_addons_array = [];
$.map(<?=  json_encode($dataformate['selectd_accessory']); ?>, function (items) {

selected_addons_array.push(parseInt(items.productid));

});

function formatState (state) {
  if (!state.id) {
    return state.text;
  }
  console.log(state);
  var $state = $(
    '<span><img style="width:10px;" src="' + state.image.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
  );
  return $state;
};
function formatedit (state) {
  if (!state.id) {
    return state.title;
  }
  console.log(state);
  var $state = $(
    '<span><img style="width:10px;" src="' + state.name.toLowerCase() + '.png" class="img-flag" /> ' + state.title + '</span>'
  );
  return $state;
};
function formatMasterProductedit(results) {


if (results.loading) {

    return results.text;
}

var markup = results.name;


return markup;
}
function SelectionMasterProductedit(results) {

return results.title;
}

$(".js-example-responsive").select2({
  data: <?=  json_encode($dataformate['selectd_accessory']); ?>,
  templateResult: formatMasterProductedit,
  templateSelection: SelectionMasterProductedit,
  escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
  ajax: {
                url: 'http://127.0.0.1:8000/Json_data',
                dataType: 'json',
                delay: 1000,
                data: function (params, page) {
                    return {
                        q: params.term, // search term
                        page: params.page || 1,

                    };
                },
                processResults: function (data, page) {
//                    console.log(data);
//                    console.log(selected_master);

                    return {
                        results: $.map(data, function (items) {

                           
                          
                           if ($.inArray(items.id,selected_addons_array) > -1 ) {


return {

    name: '<img class="img-thumbnail" width="30" src="' + items.image + '">' + items.title + ' (Product Already Used As Addon)',
    title: items.title ,
    id: items.id,
    items,
    disabled: true
}
}

                            
else{
  return {
                                name: '<img class="img-thumbnail" width="30" src="' + items.image + '">'+ items.title,
                                id: items.id,
                                title: items.title,
                                items

                            }
  
}

                           

                        }),
                        pagination: {
                            more: data.is_more_pages
                        }
                    }

                },

            },
  
  });


  
  
</script>

@endsection



