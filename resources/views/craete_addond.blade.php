
@extends('layouts.navbar')



@section('content')
<script src=
"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
      </script>
  
        <!--These jQuery libraries for 
           chosen need to be included-->
        <script src=
"https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js">
        </script>
        <link rel="stylesheet" 
              href=
"https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.min.css" />
  
        <!--These jQuery libraries for select2 
            need to be included-->
        <script src=
"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js">
       </script>
        <link rel="stylesheet" 
              href=
"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" />

<a href="" class="btn btn-primary" style="border-radius:20px;margin:10px 480px">Back to Addons List</a>
    <a href="#" class="btn btn-primary" style="border-radius: 20px;">Attach Addons to the Another Product</a>

    <div class="card " style="width:1050px;height:300px;margin-left:450px">
  <div class="card-header">
    Product Addons
  </div>
  <div class="card-body">
  <h5 class="card-title">Select Product (Master)</h5>
  <select id="js-example-data-array"class="js-example-responsive" style="width: 100%" name="master">
    <option></option></select>
  <br><br><br>
    <h5 class="card-title">Select Addons Products (Child's)</h5> 
    <meta name="csrf-token" content="{{ csrf_token() }}">
<select id="js-example-data-array1" class="js-example-responsive" multiple="multiple" style="width: 100%" name="child[]"></select>
  </div>
 
</div>

<script>

function formatState (state) {
  if (!state.id) {
    return state.text;
  }
  console.log(state);
  var $state = $(
    '<span><img style="width:10px;" src="' + state.image.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
  );
  return $state;
};

$.ajax({
  url: "http://127.0.0.1:8000/Json_data",
  cache: false,
  success: function(data){
   
$(".js-example-responsive").select2({
  data: data,
  templateResult: formatState
  })
}
});

//var selectedItems=[];

var masterData={};
var adoonData={};
$('#js-example-data-array').on('select2:select', function (e) {
     masterData.id= e.params.data.id;
     masterData.title=e.params.data.title;
     masterData.image=e.params.data.image;
     masterData.handle=e.params.data.handle;
   
    console.log(masterData);
});
$('#js-example-data-array1').on('select2:select', function (ee) {
   // var adoonData = e.params.data;
   
    adoonData.id= ee.params.data.id;      
    adoonData.title= ee.params.data.title;
    adoonData.image= ee.params.data.image;
    adoonData.handle=ee.params.data.handle;
    console.log(adoonData);

    $.ajax({
        url : "create_addon",
        method : "post",
        dataType: 'json',
        data: {

        'master_set': masterData,
        'child_set': adoonData,
        },
        success:function(response){
          console.log(response);
         
        },
       });

});

</script>

@endsection



