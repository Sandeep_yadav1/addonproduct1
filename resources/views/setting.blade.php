<!-- Font Awesome -->
@extends('layouts.navbar')



@section('content')
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
  rel="stylesheet"
/>
<!-- Google Fonts -->
<link
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
  rel="stylesheet"
/>
<!-- MDB -->
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.css"
  rel="stylesheet"
/>
<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.3.0/mdb.min.js"
></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<div class="row">
  <div class="col-3">
    <!-- Tab navs -->
    <div
      class="nav flex-column nav-tabs text-center"
      id="v-tabs-tab"
      role="tablist"
      aria-orientation="vertical"
    >
      <a
        class="nav-link active"
        id="Addon_Setting-tab"
        data-mdb-toggle="tab"
        href="#Addon_Setting"
        role="tab"
        aria-controls="v-tabs-home"
        aria-selected="true"
        >Addon Setting</a
      >
      <a
        class="nav-link"
        id="Cart_Setting-tab"
        data-mdb-toggle="tab"
        href="#Cart_Setting"
        role="tab"
        aria-controls="v-tabs-profile"
        aria-selected="false"
        >Cart Setting</a
      >
      <a
        class="nav-link"
        id="v-tabs-messages-tab"
        data-mdb-toggle="tab"
        href="#Product_Label"
        role="tab"
        aria-controls="v-tabs-messages"
        aria-selected="false"
        >Product Label</a
      >
      <a
        class="nav-link"
        id="Quick_View_Label-tab"
        data-mdb-toggle="tab"
        href="#Quick_View_Label"
        role="tab"
        aria-controls="v-tabs-messages"
        aria-selected="false"
        >Quick View Label</a
      >
      <a
        class="nav-link"
        id="Cart_Page_Label-tab"
        data-mdb-toggle="tab"
        href="#Cart_Page_Label"
        role="tab"
        aria-controls="v-tabs-messages"
        aria-selected="false"
        >Cart Page Label</a
      >
    </div>
    <!-- Tab navs -->
  </div>

  <div class="col-9">
    <!-- Tab content -->
    <div class="tab-content" id="v-tabs-tabContent">
      <div
        class="tab-pane fade show active"
        id="Addon_Setting"
        role="tabpanel"
        aria-labelledby="Addon_Setting-tab"
      >
      Product Setting<br>
    
            <label class="control-label">Maximum Addons per product</label> <br>
            <div class="col-lg-3">  
                <input type="number" class="form-control" min="1" id="max_addons" name="max_addons" value=<?php echo $resultSetting['max_addons']?>>
                </div>      
                <br>
                        <label class="control-label">Display Quantity Selector </label>
                        <br>
                        <label>
                        <input type="radio" name="quantity_selector" value="1" id="quantity_selector" {{ ($resultSetting['quantity_selector']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="quantity_selector" value="0" id="quantity_selector" {{ ($resultSetting['quantity_selector']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>  
                        <br>


                        <label class="control-label">Display Addons Image </label>
                        <br>
                        <label>
                        <input type="radio" name="addon_image" value="1" {{ ($resultSetting['addon_image']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="addon_image" value="0" {{ ($resultSetting['addon_image']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>



                        <label class="control-label">Display SoldOut Product </label>
                        <br>
                        <label>
                        <input type="radio" name="soldout_product"value="1"  {{ ($resultSetting['soldout_product']=="1")? "checked" : "" }} > <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="soldout_product" value="0"  {{ ($resultSetting['soldout_product']=="0")? "checked" : "" }} > <span class="label-text">No</span>
                        </label>   
                        <br>



                        <label class="control-label">Hyperlink the addons images to the corresponding product pages.</label>
                        <br>
                        <label>
                        <input type="radio" name="hyperlink_image" value="1" > <span class="label-text" {{ ($resultSetting['hyperlink_image']=="1")? "checked" : "" }}>Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="hyperlink_image" value="0" {{ ($resultSetting['hyperlink_image']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>



                        <label class="control-label">Show compare at price on product pages</label>
                        <br>
                        <label>
                        <input type="radio" name="compare_price" value="1" {{ ($resultSetting['compare_price']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="compare_price" value="0" {{ ($resultSetting['compare_price']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>


                        <label class="control-label">Hyperlink the addons product titles to the corresponding product pages.</label>
                        <br>
                        <label>
                        <input type="radio" name="hyperlink_title" value="1" {{ ($resultSetting['hyperlink_title']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="hyperlink_title" value="0" {{ ($resultSetting['hyperlink_title']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>



                        <label class="control-label">Make addons selectable when clicked on its image </label>
                        <br>
                        <label>
                        <input type="radio" name="selectable" value="1" {{ ($resultSetting['selectable']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="selectable" value="0" {{ ($resultSetting['selectable']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>    
                     <br>                          


                <label>
                <input type="Button" name="submit" id="submit" value="Save"  onclick="accessorySetting()"> <span class="label-text"></span>
                </label> <br>        
      </div>
      <div
        class="tab-pane fade"
        id="Cart_Setting"
        role="tabpanel"
        aria-labelledby="Cart_Setting-tab"
      >
       Cart Setting
       <label class="control-label">Maximum Addons per product</label> <br>
            <div class="col-lg-3">
                <input type="number" class="form-control" min="1" id="cart_max_addons" name="max_addons" value=<?php echo $resultSetting['cart_max_addons']?>>
                </div>      
                <br>
                        <label class="control-label">Display Quantity Selector </label>
                        <br>
                        <label>
                        <input type="radio" name="cart_quantity_selector" value="1" {{ ($resultSetting['cart_quantity_selector']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="cart_quantity_selector" value="0" {{ ($resultSetting['cart_quantity_selector']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>  
                        <br>


                        <label class="control-label">Display Addons Image </label>
                        <br>
                        <label>
                        <input type="radio" name="cart_addon_image" value="1" {{ ($resultSetting['cart_addon_image']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="cart_addon_image" value="0" {{ ($resultSetting['cart_addon_image']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>



                        <label class="control-label">Display SoldOut Product </label>
                        <br>
                        <label>
                        <input type="radio" name="cart_soldout_product" value="1" {{ ($resultSetting['cart_soldout_product']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="cart_soldout_product" value="0" {{ ($resultSetting['cart_soldout_product']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>



                        <label class="control-label">Hyperlink the addons images to the corresponding product pages.</label>
                        <br>
                        <label>
                        <input type="radio" name="cart_hyperlink_image" value="1" {{ ($resultSetting['cart_hyperlink_image']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="cart_hyperlink_image" value="0" {{ ($resultSetting['cart_hyperlink_image']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>



                        <label class="control-label">Show compare at price on product pages</label>
                        <br>
                        <label>
                        <input type="radio" name="cart_compare_price" value="1" {{ ($resultSetting['cart_compare_price']=="1")? "checked" : "" }}> <span class="label-text">Yes</span>
                        </label> <br>
                        <label>
                        <input type="radio" name="cart_compare_price" value="0" {{ ($resultSetting['cart_compare_price']=="0")? "checked" : "" }}> <span class="label-text">No</span>
                        </label>   
                        <br>


                       
                                            
                <label>
                <input type="Button" name="submit" id="submit" value="Save"  onclick="cartSetting()"> <span class="label-text"></span>
                </label> <br>        
       
      </div>
      <div
        class="tab-pane fade"
        id="Product_Label"
        role="tabpanel"
        aria-labelledby="v-tabs-messages-tab"
      >
        Product Label<br>
                  <tbody class="trans-table" id="details_page_fields">
                  <tr>
                  <td>Add Related Products (Label)</td>
                  <td><input type="text" class="form-control" id="add_related_pro_label" name="add_related_pro_label" value=<?php echo $resultSetting['add_related_pro_label']?>></td>
                  </tr>
                  <tr>
                  <td>Sold Out (Label)</td>
                  <td><input type="text" class="form-control" id="soldout_label" name="soldout_label" value=<?php echo $resultSetting['soldout_label']?>></td>
                  </tr>
                  <tr>
                  <td>Quick View Button (Label)</td>
                  <td><input type="text" class="form-control" id="viewbutton_label" name="viewbutton_label" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  </tbody>
                  </table>
                                                
                <label>
                <input type="Button" name="submit" id="submit" value="Save" onclick="prolabelsetting()"> <span class="label-text"></span>
                </label> <br>  
        
      </div>
      <div
        class="tab-pane fade"
        id="Quick_View_Label"
        role="tabpanel"
        aria-labelledby="Quick_View_Label-tab"
      >
        Quick View Label <br>
        <tbody class="trans-table" id="details_page_fields">
                  <tr>
                  <td>View Full Details (Label)</td>
                  <td><input type="text" class="form-control" id="full_detail" name="full_detail" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  <tr>
                  <td>View Less Details (Label)</td>
                  <td><input type="text" class="form-control" id="less_detail" name="less_detail" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  <tr>
                  <td>Quantity (Label)</td>
                  <td><input type="text" class="form-control" id="quantity_label" name="quantity_label" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  <tr>
                  <td>Success Message (Label)</td>
                  <td><input type="text" class="form-control" id="success_msg" name="success_msg" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  <tr>
                  <td>Variants (Label)</td>
                  <td><input type="text" class="form-control" id="variants_label" name="variants_label" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  <tr>
                  <td>Add To Cart (Label)</td>
                  <td><input type="text" class="form-control" id="addto_cartlabel" name="addto_cartlabel" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  <tr>
                  <td>Sold Out (Label)</td>
                  <td><input type="text" class="form-control" id="view_soldout_label" name="view_soldout_label" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  </tbody>
                  </table>
                                                
                <label>
                <input type="Button" name="submit" id="submit" value="Save" onclick="quickViewSetting()"> <span class="label-text"></span>
                </label> <br>  
      </div>
      <div
        class="tab-pane fade"
        id="Cart_Page_Label"
        role="tabpanel"
        aria-labelledby="Cart_Page_Label-tab"
      >
        Cart page Label 
        <tbody class="trans-table" id="details_page_fields">
                  <tr>
                  <td>Add related cart snippet heading (Label)</td>
                  <td><input type="text" class="form-control" id="snippet_heading" name="snippet_heading" value=<?php echo $resultSetting['viewbutton_label']?>></td>
                  </tr>
                  <tr>
                  <td>Add to Cart (Label)</td>
                  <td><input type="text" class="form-control" id="add_to_cartlabel" name="add_to_cartlabel" value=<?php echo $resultSetting['add_to_cartlabel']?>></td>
                  </tr>
                  </tbody>
                  </table>
                                                
                <label>
                <input type="Button" name="submit" id="submit" value="Save" onclick="cartpagelabel()"> <span class="label-text"></span>
                </label> <br>      
      </div>
    </div>
    <!-- Tab content -->
  </div>
</div>
<script>
function accessorySetting() {
  var data={'max_addons':document.getElementById("max_addons").value ,
              'quantity_selector':document.querySelector('input[name="quantity_selector"]:checked').value,
              'addon_image':document.querySelector('input[name="addon_image"]:checked').value,
              'soldout_product':document.querySelector('input[name="soldout_product"]:checked').value,
              'hyperlink_image':document.querySelector('input[name="hyperlink_image"]:checked').value,
              'compare_price':document.querySelector('input[name="compare_price"]:checked').value,
              'hyperlink_title':document.querySelector('input[name="hyperlink_title"]:checked').value,
              'selectable':document.querySelector('input[name="selectable"]:checked').value,
             
            };

    saveAjaxSetting(data);
}

function cartSetting() {
  var data={'cart_max_addons':document.getElementById("cart_max_addons").value ,
              'cart_quantity_selector':document.querySelector('input[name="cart_quantity_selector"]:checked').value,
              'cart_addon_image':document.querySelector('input[name="cart_addon_image"]:checked').value,
              'cart_soldout_product':document.querySelector('input[name="cart_soldout_product"]:checked').value,
              'cart_hyperlink_image':document.querySelector('input[name="cart_hyperlink_image"]:checked').value,
              'cart_compare_price':document.querySelector('input[name="cart_compare_price"]:checked').value,
            
             
            };

    saveAjaxSetting(data);
}

function prolabelsetting() {
  var data={'add_related_pro_label':document.getElementById("add_related_pro_label").value ,
    'soldout_label':document.getElementById("soldout_label").value ,
    'viewbutton_label':document.getElementById("viewbutton_label").value 
    
       };

    saveAjaxSetting(data);
}
function quickViewSetting() {
    var data={'full_detail':document.getElementById("full_detail").value ,
              'less_detail':document.getElementById("less_detail").value ,
              'quantity_label':document.getElementById("quantity_label").value ,
              'success_msg':document.getElementById("success_msg").value ,
              'variants_label':document.getElementById("variants_label").value ,
              'addto_cartlabel':document.getElementById("addto_cartlabel").value ,
              'view_soldout_label':document.getElementById("view_soldout_label").value 

             };

    saveAjaxSetting(data);
}
function cartpagelabel() {
    var data={'snippet_heading':document.getElementById("snippet_heading").value ,
              'add_to_cartlabel':document.getElementById("add_to_cartlabel").value ,
             };

    saveAjaxSetting(data);
}
function saveAjaxSetting(resultSetting) { 
    $.ajax({
        type: "POST",
        url: "/store",
        dataType: "json",
        data: resultSetting,
        success: function (data) {
           
        },
       
    });
}

</script>
@endsection
