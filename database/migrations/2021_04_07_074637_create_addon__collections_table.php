<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddonCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                Schema::create('addon__collection', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->bigInteger('collection_id')->lenght(10)->unsigned();
                    $table->bigInteger('addon_id')->lenght(10)->unsigned();
                    $table->foreign('collection_id')->references('id')->on('collections')->onDelete('cascade');
                    $table->foreign('addon_id')->references('id')->on('addons')->onDelete('cascade');
                    $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addon__collections');
    }
}
