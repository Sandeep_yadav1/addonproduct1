<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/craete_addond', function () {
    return view('craete_addond');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/Json_data', 'DataController@index');


Route::post('/create_addon', 'AddonProductControler@store');//addon_list
Route::get('/addon_list', 'AddonProductControler@index');//get_list
Route::get('/get_list', 'AddonProductControler@show');//productEdit
Route::get('/productEdit/{id}', 'AddonProductControler@edit');//SettingController
Route::get('/setting', 'SettingController@index');//store
Route::post('/store', 'SettingController@store');//image_store.blade

Route::get('/image', function () {
    return view('image_store');
});

Route::post('/image', 'ImageController@store');//getimage


Route::get('/foo', function () {

   $re= Artisan::call('change:name 1 --que=hi');
   return $re;

    //
});

Route::get('/send-test-email', 'EmailController@sendEmail');