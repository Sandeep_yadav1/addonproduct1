<?php
namespace App\Library;

use App\Library\Product;
use App\Library\Addon;
use App\Models\Product as Model;
use App\Models\Addon as Model1;

class AddonProdcut
{
    public static function AddonStore($data)
    {
        try
        {
           
            $masterData= $data['master_set'];
            $addonData=$data['child_set'];

           $productobj= Product::productStore($masterData);
       
        
           
           $addonid= Addon::addonStore($addonData);
 
            
           $productobj->addons()->attach($addonid);

        }
        catch(Exception $e)
            {
                throw new Exception("oops Somthing went Wrong");
            }
       
      
    }

    public static function productEdit($id)
    {
        try
        {
            $data = Model::with('addons')->find($id)->toArray();	
            
          
            return    $data ;
        }
        catch(Exception $e)
        {
            throw new Exception("oops Somthing went Wrong");
        }
    }




}
?>