<?php
namespace App\Library;
use App\Models\Setting as Model;
use App\Models\DefaultData;
class Setting
{
    protected static $defaultSetting=[ 
    'max_addons' => '5',
    'quantity_selector' => '0',
    'addon_image' => '0',
    'soldout_product' => '0',
    'hyperlink_image' => '0',
    'compare_price' => '0',
    'hyperlink_title' => '0',
    'selectable' => '0',
    'cart_max_addons' => '2',
    'cart_quantity_selector' => '0',
    'cart_addon_image' => '0',
    'cart_soldout_product' => '0',
    'cart_hyperlink_image' => '0',
    'cart_compare_price' => '0',
    'add_related_pro_label' => 'Product Addons b',
    'soldout_label' => 'Sold Out b',
    'viewbutton_label' => 'Quick View b',
    'full_detail' => 'full detail',
    'less_detail' => 'less detail',
    'quantity_label' => 'Quantity',
    'success_msg' => 'Product Added to cart',
    'variants_label' => 'variants',
    'addto_cartlabel' => 'Add to Cart',
    'view_soldout_label' => 'SoldOut',
    'snippet_heading' => 'cart addon box',
    'add_to_cartlabel' => 'add addon'];
    public static function saveSetting($data):bool
    {
        try
        {
            $dbData= Model::select('entity_name','entity_value')->get()->toArray();
            $dbDatacol= array_column($dbData, 'entity_value', 'entity_name');
            $matchedValues=array_intersect_key($dbDatacol,$data);
            $entityKey=array_keys($matchedValues);

            foreach($data as $key=>$value)
             {
                $data1[]=['entity_name'=>$key,'entity_value'=>$value];
             }

            if(!empty($result))
             {
                Model::whereIn('entity_name',array_values($entityKey))->delete();
             }

            Model::insert($data1);
            return true;
        }
        catch(Exception $e)
        {
            throw new Exception("oops Somthing went wrong");
        }
    }

    public static function getSetting():array
    {
        try
        {
            $dbSetting=Model::select('entity_name', 'entity_value')->get()->toArray();
            $dbColumns=array_column($dbSetting, 'entity_value','entity_name');
            $dataSetting=array_merge(self::$defaultSetting,$dbColumns);
            return $dataSetting;
        }
        catch(Exception $e)
        {
            \Log::info($e);
            return [];
            throw new Excetion("oops somthing wen wrong");
        }
        
    }  
}


?>
 
    