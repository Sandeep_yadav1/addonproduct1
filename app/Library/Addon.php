<?php
namespace App\Library;
use App\Models\Addon as Model;

class Addon
{
    public static function addonStore($data):int
    {
        try
        {

        

           $addondata= Model::where('productid','=',$data['id'])->first();
          
           if( $addondata==null)
           {
            $addonobj=new Model;
            $addonobj->productid=$data['id'];
            $addonobj->title=$data['title'];
            $addonobj->image=$data['image'];
            $addonobj->handle=$data['handle'];
            $addonobj->save();
           return $addonobj->id;
           }
           else
           {
               return   $addondata['id'];
           }
          
        }
      
        catch(Exception $e) 
          {
            throw new Exception("oops Somthing went Wrong");
          }
    }
}



?>