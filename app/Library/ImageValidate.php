<?php

namespace App\Library;

class ImageValidate
{
    public static function imageValidation($request)
    {
        // dd($request);
        try
        {
            $request->validate([
                'image1' => 'max:20|mimes:jpeg,png', 
            ]);
            
            
        }
        catch(Exception $e)
        {
            \Log::info($e);
            return "";
        }
    }
}

?>