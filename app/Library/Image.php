<?php

namespace App\Library;
use App\Library\ImageValidate;

use Illuminate\Http\File;

use Illuminate\Support\Facades\Storage;
use App\Models\Addon as Model;

use App\Library\Image;

class Image
{
    public static function storeImage($data):bool
    {
        try 
        {
          
            $imageName=$data->file('image1')->getClientOriginalName();
            $imageContent=$data->file('image1')-> getPathname();
            $content=file_get_contents($imageContent);
      
         
          $nameConverted= md5($imageName);
         $checkStatus= self::checkImage($nameConverted);
         if( !$checkStatus)
         {
            Storage::put($nameConverted, $content);
         }
          \Log::info($checkStatus);

          return true;
        }
        catch(Exception $e)
        {
            \Log::info($e);
            return false;
        }
    }
    public static function getImage():string
    {
       try
       {

       }
       catch(Exception $e)
       {
           \Log::info($e);
           return "";
       }
    }

    public static function checkImage($name):bool
    {
        try
        {
           $exists = Storage::exists(($name));
           return $exists;
        }
        catch(Exception $e)
        {
            \Log::info($e);
            return false;
        }
    }
}
?>