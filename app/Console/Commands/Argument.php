<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Argument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'argument:name {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'it is for only fir practice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data=$this->argument('user');
        $this->info('this is the passed argument by the user '.$data);
    }
}
