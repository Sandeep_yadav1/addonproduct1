<?php

namespace App\Console\Commands;

use App\Models\Setting;

use Illuminate\Console\Command;

class Practice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:name {user=} {--que=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $queueName = $this->option('que');
        \Log::info($queueName);
      
    
    }
}
