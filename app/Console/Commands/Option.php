<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Option extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'option {user*} {--queue=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $data=$this->option();

        \Log::info($arguments);
        \Log::info($data);
       //$this->info('this is only for practice '.$data.' '.$arguments);
    }
}
