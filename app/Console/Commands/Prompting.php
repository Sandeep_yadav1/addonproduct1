<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Prompting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'prompting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $name=$this->ask('pls enter ur name');
       $this->info(' hi you have entered that your name is '.$name);
       if ($this->confirm('Do you wish to continue,entering your name if want to enter name then just press y or ?')) {
        $namecnf=$this->ask('pls enter ur name');
        \Log::info($namecnf);
    }
       \Log::info($name);
    }
}
