<?php

namespace App\Console\Commands;

use App\Models\Setting;

use Illuminate\Console\Command;

class ChangeName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:name {user=414} {--changeName=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It is for changging the Name in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $userId = $this->argument('user');
       $changeName = $this->option('changeName');
       $oldName=Setting::find( $userId);
       $oldName->entity_value= $changeName;
       $oldName->save();
        $this->info('The Name has been Changed in Database to '.$changeName);
    }
}
