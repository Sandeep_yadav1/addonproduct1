<?php

namespace App\Console\Commands;
use App\Models\Change;
use App\Jobs\ChangeShopEmail;

use Illuminate\Console\Command;

class ChangeShopName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'changeshopname {shopName} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this is for changing the email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('shopName');
        $email=$this->argument('email');
        $data= Change::where('shopName', $name)->first()->toArray();
        $jsondata['data']= ['id'=>$data['id'],'shopname'=>$data['shopname'],'email'=> $email];
        ChangeShopEmail::dispatch($jsondata);
    }
}
