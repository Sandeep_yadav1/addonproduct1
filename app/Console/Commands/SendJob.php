<?php

namespace App\Console\Commands;

use App\Jobs\SendData;

use Illuminate\Console\Command;

class SendJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:data {data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data=$this->argument('data');
        $this->info($data);
        SendData::dispatch($data)->delay(now()->addMinutes(1));;
    }
}
