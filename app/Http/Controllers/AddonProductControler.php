<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\AddonProdcut;
use App\Library\Addonlist;
use App\Product;

class AddonProductControler extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("addon_list");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            AddonProdcut::AddonStore($request);
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors(['message'=>'Something went wrong']);
        }
      
     
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try
        {
           // \Log::info(  $request);
           $result= Addonlist::addonShow($request);
          $formateddata=['total'=> $result['total'],'rows'=> $result['data']];
         $jsondata= json_encode( $formateddata);
          \Log::info(  $jsondata);
           return   $jsondata;
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors(['message'=>'Something went wrong']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $result=AddonProdcut::productEdit($id);
        \Log::info( $result);
        $child=[];
      foreach( $result['addons'] as $addons)
    {
        $child[]=['id'=>$addons['id'],'title'=>$addons['title'],'selected'=>"true",'name'=>$addons['image'],'productid'=>$addons['productid'],'item'=>$addons['pivot']];
      
    }
 
    
$dataformate=['master_product'=>$result['id'],'title'=>$result['title'],'selectd_accessory'=>$child];

$jsondata=json_encode($dataformate);
\Log::info($dataformate);


        return view("productEdit",compact('dataformate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
