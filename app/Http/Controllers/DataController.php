<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //\Log::info($request);
        //$data=[['title'=>'Adidas bag1','images'=>'https://ibb.co/5MY84TY','productid'=>'25','handle'=>'bag1',],['title'=>'Adidas bag1','images'=>'hhttps://ibb.co/5MY84TYi','productid'=>'25','handle'=>'bag1',]];
        $data1 = [
            [
                'id'=> 1,
                'text'=> 'Adidas bag1',
                'image'=>'https://images.pexels.com/photos/799443/pexels-photo-799443.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                'title'=>'Adidas bag1',
                'handle'=>' handle Adidas bag1'
            ],
            [
                'id'=> 11,
                'text'=> 'Adidas bag2',
                'image'=>'https://images.pexels.com/photos/719396/pexels-photo-719396.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                'title'=>'Adidas bag2',
                'handle'=>'handle Adidas bag2'
            ],
            [
                'id'=> 21,
                'text'=> 'Adidas bag3',
                'image'=>'https://images.pexels.com/photos/719396/pexels-photo-719396.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                'title'=>'Adidas bag3',
                'handle'=>'handle Adidas bag3'
            ],  
            [
                'id'=> 31,
                'text'=> 'Adidas bag4',
                'image'=>'https://images.pexels.com/photos/719396/pexels-photo-719396.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                'title'=>'Adidas bag4',
                'handle'=>'handle Adidas bag4'
            ],
            [
                'id'=> 41,
                'text'=> 'Adidas bag5',
                'image'=>'https://images.pexels.com/photos/719396/pexels-photo-719396.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                'title'=>'Adidas bag5',
                'handle'=>'handle Adidas bag5'
               ]
            
        ];
        
    return $data1; 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
