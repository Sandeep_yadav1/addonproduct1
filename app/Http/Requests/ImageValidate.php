<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'image1' =>'mimes:png,pbm,gif,svg|max:5',
        ];
    }
    public function attributes()
    {
        return [
            'image1' => 'image',
        ];
    }  

//     public function messages()
// {
//     return [
//         'image1.max' => 'A title is required',
//         'body.required'  => 'A message is required',
//     ];
// }

    
}
