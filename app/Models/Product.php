<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function addons()
    {
        return $this->belongsToMany(Addon::class, 'addon__product');
    }
}
