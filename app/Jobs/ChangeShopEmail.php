<?php

namespace App\Jobs;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Route;
use App\Models\Change;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ChangeShopEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $jsonData;
    public function __construct($data)
    {
       
        $this->jsonData=$data;
    }

    /**
     * Execute the job.
     *
     * @return voids
     */
    public function handle()
    {
        $id=$this->jsonData['data']['id'];
        $data= Change::find($id);
        $data['email']=$this->jsonData['data']['email'];
        $data->save();

    }
}
