<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function retryUntil()
    {
        return now()->addMinutes(2);
    }
    public $tries = 3;
    public $timeout = 5;
    protected $jobData;
    public function __construct($data)
    {
        //\Log::info($data);
     // $this->jobData=$data;
     
    }   

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       
       // \Log::info($this->jobData);
    }
}
